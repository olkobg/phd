source 'https://rubygems.org'
ruby '2.1.5'

gem 'bootstrap-sass'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
gem 'devise'

gem 'elasticsearch-model'
gem 'elasticsearch-rails'
gem 'bonsai-elasticsearch-rails'

gem 'filepicker-rails'
gem 'factory_girl_rails'
gem 'faker'
gem 'figaro'
gem "haml-rails", "> 0.4.0"
# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'rails-i18n', '~> 4.0.0' # For 4.0.x
gem 'spreadsheet'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'
gem 'pg'
gem 'puma'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.9'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

group :production do
  gem 'rails_12factor'
end

gem 'newrelic_rpm'

group :development, :test do
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'database_cleaner'
  gem 'poltergeist'
  gem 'rspec-rails'  
  gem 'sqlite3'
  gem 'shoulda-matchers', require: false
  gem 'simplecov', require:  false
end

group :development do
  gem 'pry-rails'
  gem 'pry-byebug'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use debugger
# gem 'debugger', group: [:development, :test]
