require 'feature_helper'

RSpec.feature 'Login', type: :feature do

  scenario 'on root page' do
    visit root_path
    visit new_user_session_path

    expect(login_email).not_to be_nil
    expect(login_password).not_to be_nil
  end

  scenario 'with password and name' do
    admin = FactoryGirl.create :admin
    login admin

    expect(page).to have_content I18n.t('devise.sessions.user.signed_in')
    expect(page).to have_content I18n.t('search')
    expect(page).to have_content I18n.t('result')
  end

  context 'with google account' do
    pending 'need help with test account creation\remove'
  end

  scenario 'do not allow user login with wrong password' do
    admin = FactoryGirl.create :admin

    admin.password = admin.password + 'abc'
    login admin

    expect(page).to have_content I18n.t 'devise.failure.user.invalid'
  end

  scenario "sign_up feature dissabled" do
    expect { visit '/users/sign_up' }.to raise_error
  end

end
