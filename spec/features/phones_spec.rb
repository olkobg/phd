require 'feature_helper'

RSpec.feature 'Phones', type: :feature do

  before do
    @folder = FactoryGirl.create :folder do |folder|
      folder.phones << FactoryGirl.create(:phone)
    end
    @admin = FactoryGirl.create :admin
  end

  scenario 'show when description nil' do
    login(@admin)
    visit folder_path(@folder)
    expect(page).to have_content @folder.phones.first.phoneNumber
  end

  context "show label" do
    ['fax', 'mobile', 'land', 'mobile'].each do |phone_type|
      scenario "of #{phone_type}" do
        @test_folder = FactoryGirl.create :folder do |folder|
          folder.phones << FactoryGirl.create(:phone, description: phone_type)
        end
        login @admin
        visit folder_path(@test_folder)
        expect(page).to have_content I18n.t("folder.contact.phones.#{phone_type}")
      end
    end
  end
end
