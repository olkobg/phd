require 'feature_helper'

RSpec.feature 'Avatar', type: :feature do
  before do
    admin = FactoryGirl.create :admin
    @folder = FactoryGirl.create :test_folder
    @folder_with_avatar = FactoryGirl.create :test_folder_with_avatar
    login(admin)
  end

  scenario 'upload button displayed with no photo' do
    visit folders_path
    within :xpath, "//strong[contains(text(), '#{@folder.firstName} #{@folder.lastName}')]/../.." do
      click_on I18n.t('folder.user_details')
    end
    expect(page).to have_xpath "//input[contains(@name, 'avatar_url') and contains(@data-fp-button-text, '#{I18n.t('folder.photo.choose')}')]"
  end

  scenario 'when image  added show change ' do
    visit folders_path
    within :xpath, "//strong[contains(text(), '#{@folder_with_avatar.firstName} #{@folder_with_avatar.lastName}')]/../.." do
      click_on I18n.t('folder.user_details')
    end
    expect(page).to have_xpath "//input[contains(@name, 'avatar_url') and contains(@data-fp-button-text, '#{I18n.t('folder.photo.change')}')]"
  end
  
  context 'change avatar' do
    pending 'have free filepicker account'
  end
end