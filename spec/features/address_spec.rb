require 'feature_helper'

RSpec.feature 'Address', type: :feature do
  before do
    @admin = FactoryGirl.create :admin
  end

  context "not shown" do
    ['address', 'webPage'].each do |parametr|
      scenario "when #{parametr} is nil" do
        folder = FactoryGirl.create :folder do |folder|
          folder.send("#{parametr}=".to_sym, nil)
        end
        folder.save

        login @admin
        visit folder_path(folder)

        expect(page).not_to have_content I18n.t("folder.contact.#{parametr}")
      end
    end
  end
end