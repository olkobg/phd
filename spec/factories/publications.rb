FactoryGirl.define do
  factory :publication, class: Publication do
    publicationId { Faker::Number.number(3) }
    folderId Faker::Number.number(3)
    name Faker::Lorem.word
    date Faker::Time.between(12.years.ago, Time.now)
  end

  factory :test_publication, parent: :publication do
    name 'testPublication'
  end

  factory :other_test_publication, parent: :publication do
    name 'OtherTestPUblication'
  end
end