FactoryGirl.define do
  factory :dissertation, class: Dissertation do
    dissertationId { Faker::Number.number(3) }
    folderId Faker::Number.number(3)
    startDate Time.now
    finishDate Time.now
    thesisDate Time.now
    title Faker::Lorem.sentence(3)
    educationLevel 'high'
    mentorId Faker::Number.number(3)
    educationPlace Faker::Lorem.sentence(4)
    notes Faker::Lorem.paragraph
  end

  factory :test_dissertation, parent: :dissertation do
    dissertationId { Faker::Number.number(3) }
    title "my test dissertation"
    educationLevel 'high'
    educationPlace 'Збараж'
    notes 'просто тестовий опис'
  end
end