FactoryGirl.define do
  factory :lang, class: Language do
    languageId { Faker::Number.number(3) }
    name 'Ukrainian'
  end

  factory :lang_eng, parent: :lang do
    name 'English'
  end

  factory :lang_pol, parent: :lang do
    name 'Polish'
  end
end