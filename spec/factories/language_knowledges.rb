FactoryGirl.define do
  factory :language_knowledge, class: LanguageKnowledge do
    folderId { Faker::Number.number(3) }
    level 'good'
    association :language, factory: :lang
  end
end
