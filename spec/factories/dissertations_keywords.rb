FactoryGirl.define do
  factory :dissertations_keywords, class: DissertationsKeywords do
    dissertationId Faker::Number.number(3)
    keywordId Faker::Number.number(3)    
  end
end
