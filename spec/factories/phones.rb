FactoryGirl.define do
  factory :phone, class: Phone do
    phoneId { Faker::Number.number(3) }
    folderId Faker::Number.number(3)
    phoneNumber Faker::PhoneNumber.cell_phone
    description 'folder.contact.phones.phone'
  end

  factory :phone_fax, parent: :phone do
    description 'folder.contact.phones.fax'
  end

  factory :phone_land, parent: :phone do
    description 'folder.contact.phones.land'
  end

  factory :phone_mobile, parent: :phone do
    description 'folder.contact.phones.mobile'
  end

end