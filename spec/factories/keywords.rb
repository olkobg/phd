FactoryGirl.define do
  factory :keyword, class: Keyword do
    keywordId Faker::Number.number(3)
    name Faker::Lorem.word
  end
end