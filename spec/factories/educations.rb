FactoryGirl.define do
  factory :education, class: Education do
    folderId { Faker::Number.number(3) }
    educationId { Faker::Number.number(3) }
    startDate Time.now - 15.years
    endDate Time.now - 10.years
    place Faker::Lorem.sentence(3)
    educationLevel 'folder.edication.low'
  end

  factory :test_education, parent: :education do
    place 'testeducaitonplace'
  end

  factory :education_high, parent: :test_education do
    educationLevel 'folder.edication.high'
  end

  factory :education_middle, parent: :test_education do
    educationLevel 'folder.edication.middle'
  end
end