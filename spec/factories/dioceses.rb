FactoryGirl.define do
  factory :diocese, class: Diocese do
    dioceseId { Faker::Number.number(3) }
    name 'Test Diocese'
    description { Faker::Lorem.sentence }

    factory :other_diocese do
      name 'Other Diocese'
    end
  end
end