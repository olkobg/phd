FactoryGirl.define do
  factory :test_email, aliases: [:email], class: Email do
    email Faker::Internet.email
    emailId { Faker::Number.number(3) }
    folderId Faker::Number.number(3)
  end
end 