FactoryGirl.define do
  factory :monk_community do
    monkCommunityId { Faker::Number.number 3 }
    name 'Test Monk Community'
  end

  factory :other_monk_community, parent: :monk_community do
    name 'Othercommunity'
  end
end