FactoryGirl.define do
  factory :user do
    email 'usr@usr.com'
    password 'password'
  end

  factory :admin, class: User, parent: :user do
    email 'test_admin@adm.com'
  end
end
