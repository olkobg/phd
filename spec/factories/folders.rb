address_data = [
  Faker::Address.city,
  Faker::Address.street_address,
  Faker::Address.secondary_address,
  Faker::Address.zip,
  Faker::Address.state
].join(', ');

FactoryGirl.define do
  factory :folder, class: Folder do
    folderId { Faker::Number.number(3) }
    firstName Faker::Name.first_name
    lastName Faker::Name.last_name
    sex 'male'
    note Faker::Lorem.sentences(10).join('. ')
    perpetualVowsDate Time.now - 10.years
    dioceseId Faker::Number.number(3)
    monkCommunityId Faker::Number.number(3)
    address address_data
    webPage Faker::Internet.url('localhost:3000')

    # transient  do
    #   languages
    # end

    # after(:create) do |user, evaluator|
    #   create_list(:language_knowledge, evaluator.languages, )
    # end

    trait :nil_perpetual_vows_date do
      perpetualVowsDate nil
    end
  end

  factory :folder_female, parent: :folder do
    sex 'female'
  end

  factory :test_folder, parent: :folder do
    firstName 'Андрій'
    lastName 'Петренко'
    note 'some beautiful note'
  end

  factory :test_folder_with_avatar, parent: :test_folder do
    firstName 'Петро'
    lastName 'Андрієнко'
    avatar_url 'https://www.filepicker.io/api/file/RE2NT0WPTSq685eO0Lb7'
  end

  factory :test_folder_all_stuff, parent: :test_folder do
    firstName 'Василь'
    lastName 'Прокопенко'
  end

  factory :folder_perpetual_dates_nil, parent: :folder, traits: [:nil_perpetual_vows_date]

end