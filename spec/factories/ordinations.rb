FactoryGirl.define do
  factory :ordination, class: Ordination do
    ordinationId { Faker::Number.number(3) }
    bishopId Faker::Number.number(3)
    folderId Faker::Number.number(3)
    date Time.now - 7.years
    ordinationType 'folder.ordination.low'
  end

  factory :ordination_diacon, parent: :ordination do
    ordinationType 'folder.ordination.diacon'
  end

  factory :ordination_prist, parent: :ordination do
    ordinationType 'folder.ordination.diacon'
  end

  factory :ordination_bishop, parent: :ordination do
    ordinationType 'folder.ordination.diacon'
  end
end