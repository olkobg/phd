FactoryGirl.define do
  factory :experience, class: Experience do
    folderId Faker::Number.number(3)
    experienceId { Faker::Number.number(3) }
    startDate Time.now
    endDate Time.now
    place Faker::Lorem.sentence
    position Faker::Lorem.word
  end

  factory :experience_first, parent: :experience do
    place 'placeone'
    position 'posone'
  end

  factory :experience_second, parent: :experience do
    place 'placetwo'
    position 'postwo'
  end
end