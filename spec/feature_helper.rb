require "rails_helper"
def login(user)
  visit root_path
  visit new_user_session_path
  login_email.set user.email
  login_password.set user.password
  login_button.click
end

def logout
  visit_delete_user_session
end

def login_email
  find("input[placeholder='#{I18n.t('email.placeholder')}']")
end

def login_password
  find("input[placeholder='#{I18n.t('password.placeholder')}']")
end

def login_button
  find('input.btn.btn-success.btn-login-width')
end