require 'rails_helper'

RSpec.describe Dissertation, type: :model do
  before do
    @dissertation = FactoryGirl.build :dissertation
  end

  context "validations" do
    it { expect(@dissertation).to belong_to(:folder) }
    it { expect(@dissertation).to have_one(:mentor) }
    it { expect(@dissertation).to have_and_belong_to_many(:keywords) }
  end
end
