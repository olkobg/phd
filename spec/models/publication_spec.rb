require 'rails_helper'

RSpec.describe Publication, type: :model do
  before do
    @publication = FactoryGirl.build :publication
  end

  context "validations" do
    it { expect(@publication).to belong_to(:folder) }
    # it { should belong_to :folder }
  end
end
