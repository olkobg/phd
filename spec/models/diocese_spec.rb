require 'rails_helper'

RSpec.describe Diocese, type: :model do
  before do
    # TODO: move all the staff to use let clause instead of...
    @diocese = FactoryGirl.create :diocese
  end
  
  context "validations" do
    it { expect(@diocese).to have_many(:folders) }
  end

  context "elastic search" do
    context "folder" do
      context "add" do
        pending
      end
      context "remove" do
        pending
      end
      context "change" do
        pending
      end
    end
  end
end
