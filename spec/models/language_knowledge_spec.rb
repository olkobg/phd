require 'rails_helper'

RSpec.describe LanguageKnowledge, type: :model do
  before do
    @lang_knowledge = FactoryGirl.build :language_knowledge
  end

  context "validations" do
    it { expect(@lang_knowledge).to belong_to(:folder) }
    it { expect(@lang_knowledge).to belong_to(:language) }

    # it { should belong_to :folder }
    # it { should belong_to :language }
  end
end
