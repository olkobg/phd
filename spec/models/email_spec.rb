require 'rails_helper'

RSpec.describe Email, type: :model do
  before do
    @email = FactoryGirl.build :email
  end

  context "validations" do
    it { expect(@email).to belong_to(:folder) }
  end
end
