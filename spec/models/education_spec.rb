require 'rails_helper'

RSpec.describe Education, type: :model do
  before do
    @education = FactoryGirl.build :education
  end

  it { expect(@education).to belong_to(:folder) }
end