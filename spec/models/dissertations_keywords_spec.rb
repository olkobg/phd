require 'rails_helper'

RSpec.describe DissertationsKeywords, type: :model do
  before do
    @diss_keywords = FactoryGirl.build :dissertations_keywords
  end

  context "validations" do
    it { expect(@diss_keywords).to belong_to(:dissertation) }
    it { expect(@diss_keywords).to belong_to(:keyword) }

    # it { should belong_to(:dissertation) }
    # it { should belong_to(:keyword) }
  end
end
