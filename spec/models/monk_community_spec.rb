require 'rails_helper'

RSpec.describe MonkCommunity, type: :model do
  before do
    @monk_community = FactoryGirl.build :monk_community
  end

  context "validations" do
    it { expect(@monk_community).to have_many(:folders) }
    # it { should have_many :folders }
  end
end
