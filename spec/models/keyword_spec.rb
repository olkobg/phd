require 'rails_helper'

RSpec.describe Keyword, type: :model do
  before do
    @keyword = FactoryGirl.build :keyword
  end

  context "validations" do
    it { expect(@keyword).to have_and_belong_to_many(:dissertations) }
    # it { should have_and_belong_to_many :dissertations }
  end
end
