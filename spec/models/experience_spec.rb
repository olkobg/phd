require 'rails_helper'

RSpec.describe Experience, type: :model do
  before do
    @experience = FactoryGirl.build :experience
  end

  context "validations" do
    it { expect(@experience).to belong_to(:folder) }
    # it { should belong_to :folder }
  end
end
