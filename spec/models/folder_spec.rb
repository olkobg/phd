require 'rails_helper'

RSpec.describe Folder, type: :model do
  
  before { FactoryGirl.build(:folder) }

  context "validation" do
    folder = FactoryGirl.create :folder
    it { expect(folder).to belong_to(:diocese) }
    it { expect(folder).to belong_to(:monk_community) }
    it { expect(folder).to have_many(:dissertations) }
    it { expect(folder).to have_many(:educations) }
    it { expect(folder).to have_many(:experiences) }
    it { expect(folder).to have_many(:language_knowledges) }
    it { expect(folder).to have_many(:languages).through(:language_knowledges)}
    it { expect(folder).to have_many(:ordinations) }
    it { expect(folder).to have_many(:phones) }
    it { expect(folder).to have_many(:emails) }
    it { expect(folder).to have_many(:publications) }
  end

  context 'with nil values of' do
    context "perpetuals dates" do
      pending "fix error"
      # expect { FactoryGirl.create :folder_perpetual_dates_nil}.to raise_error
    end
  end

  describe 'check folder index for elasticsearch update after' do
    let(:folder) { FactoryGirl.create :test_folder }

    before do
      folder
    end

    before :each do
      Folder.__elasticsearch__.client.indices.delete index: Folder.index_name rescue nil
      Folder.import
    end

    # TODO: make single test for repeatable params [:firstName, :lastName, :address]
    context "changed" do
      before :each do

        Folder.__elasticsearch__.refresh_index!
      end

      ['firstName', 'lastName', 'address'].each do |param|
        it param do
          response = Folder.search
          expect(response.results.total).to eq 1

          folder.send((param + '=').to_sym, 'otherFirstNames')
          folder.save
          Folder.__elasticsearch__.refresh_index!

          response = Folder.search folder.folder.send(param.to_sym)
          expect(response.results.total).to eq 1
        end
      end

      # it "firstName" do
      #   response = Folder.search folder.firstName
      #   expect(response.results.total).to eq 1

      #   folder.firstName = 'otherFirstName'
      #   folder.save
      #   Folder.__elasticsearch__.refresh_index!

      #   response = Folder.search folder.firstName
      #   expect(response.results.total).to eq 1
      # end

      # it "lastName" do
      #   response = Folder.search folder.lastName
      #   expect(response.results.total).to eq 1

      #   folder.lastName = 'otherLastName'
      #   folder.save
      #   Folder.__elasticsearch__.refresh_index!

      #   response = Folder.search folder.lastName
      #   expect(response.results.total).to eq 1
      # end

      # it "address" do
      #   response = Folder.search folder.address
      #   expect(response.results.total).to eq 1

      #   folder.address = 'otherAddress'
      #   folder.save
      #   Folder.__elasticsearch__.refresh_index!

      #   response = Folder.search folder.address
      #   expect(response.results.total).to eq 1
      # end
    end

    context "language" do
      before :each do
        @lang = FactoryGirl.create :lang
        folder.languages << @lang
        folder.save
        Folder.__elasticsearch__.refresh_index!
      end

      it "added" do
        response = Folder.search @lang.name
        expect(response.results.total).to eq 1
      end

      it "changed " do
        response = Folder.search @lang.name
        expect(response.results.total).to eq 1

        old_lang_name = @lang.name
        @lang.update_attributes(name: FactoryGirl.build(:lang_eng).name)
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search old_lang_name
        expect(response.results.total).to eq 0
        response = Folder.search @lang.name
        expect(response.results.total).to eq 1
      end

      it "removed" do
        lang_name = @lang.name
        response = Folder.search lang_name
        expect(response.results.total).to eq 1
        Language.destroy(@lang.languageId)
        folder.reload
        Folder.__elasticsearch__.refresh_index!
        response = Folder.search lang_name
        expect(response.results.total).to eq 0
      end

      it "removed through collections" do
        folder.languages << @lang
        folder.save

        Folder.__elasticsearch__.refresh_index!

        lang_name = @lang.name
        response = Folder.search lang_name
        expect(response.results.total).to eq 1
        folder.languages.destroy(@lang)
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search lang_name
        expect(response.results.total).to eq 0
      end

    end

    context "diocese" do
      before :each do
        @diocese = FactoryGirl.create :diocese
        folder.diocese=@diocese
        folder.save
        Folder.__elasticsearch__.refresh_index!
      end
      it "added" do
        response = Folder.search @diocese.name
        expect(response.results.total).to eq 1
      end

      it "changed" do
        @other_diocese = FactoryGirl.create :other_diocese
        folder.diocese = @other_diocese
        folder.save
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @diocese.name
        expect(response.results.total).to eq 1
      end

      it "removed" do
        folder.diocese = nil
        folder.save
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @diocese.name
        expect(response.results.total).to eq 0
      end
    end

    context "dissertations" do
      before :each do
        @dissertation = FactoryGirl.create :test_dissertation
        folder.dissertations << @dissertation
        folder.save
        Folder.__elasticsearch__.refresh_index!
      end
      it "added" do
        response = Folder.search @dissertation.title
        expect(response.results.total).to eq 1
      end
      it "changed" do
        response = Folder.search @dissertation.title
        expect(response.results.total).to eq 1

        @dissertation.title = 'another diss title'
        @dissertation.save
        folder.save
        Folder.__elasticsearch__.refresh_index!
        response = Folder.search @dissertation.title
        expect(response.results.total).to eq 1
      end
      it "removed" do
        response = Folder.search @dissertation.title
        expect(response.results.total).to eq 1

        folder.dissertations.destroy(@dissertation)
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @dissertation.title
        expect(response.results.total).to eq 0
      end
    end

    context "education" do
      before :each do
        @education = FactoryGirl.create :test_education
        folder.educations << @education
        folder.save
        Folder.__elasticsearch__.refresh_index!
      end

      it "added" do
        response = Folder.search @education.place
        expect(response.results.total).to eq 1
      end

      context "changed" do
        it "place" do
          response = Folder.search @education.place
          expect(response.results.total).to eq 1

          @education.place = 'othereducationplace'
          Folder.__elasticsearch__.refresh_index!

          response = Folder.search @education.place
          expect(response.results.total).to eq 0
        end
        it "educationLevel" do
          response = Folder.search @education.educationLevel
          expect(response.results.total).to eq 1

          @education.educationLevel = 'superhigh'
          Folder.__elasticsearch__.refresh_index!

          response = Folder.search @education.educationLevel
          expect(response.results.total).to eq 0
        end
      end


      it "removed" do
        response = Folder.search @education.place
        expect(response.results.total).to eq 1

        folder.educations.destroy(@education)
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @education.place
        expect(response.results.total).to eq 0
      end
    end

    context "experience" do
      before :each do
        @experience = FactoryGirl.create(:experience_first)
        folder.experiences << @experience
        folder.save
        Folder.__elasticsearch__.refresh_index!
      end
      it "added" do
        response = Folder.search  @experience.position
        expect(response.results.total).to eq 1
      end

      context "changed" do
        it "position" do
          @experience.position = 'otherposition'
          @experience.save
          folder.save
          Folder.__elasticsearch__.refresh_index!

          response = Folder.search  @experience.position
          expect(response.results.total).to eq 1
        end
        it "place" do
          @experience.place = 'otherplace'
          @experience.save
          folder.save
          Folder.__elasticsearch__.refresh_index!

          response = Folder.search  @experience.place
          expect(response.results.total).to eq 1
        end
      end

      it "removed" do
        folder.experiences.destroy(@experience)
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @experience.place
        expect(response.results.total).to eq 0

        response = Folder.search @experience.position
        expect(response.results.total).to eq 0
      end
    end
    context "monk community" do
      before :each do
        @monk_community = FactoryGirl.create :monk_community
        folder.monk_community = @monk_community
        folder.save
        Folder.__elasticsearch__.refresh_index!
      end
      it "added" do
        response = Folder.search @monk_community.name
        expect(response.results.total).to eq(1)
      end

      it " changed" do
        @other_monk_community = FactoryGirl.create :other_monk_community
        folder.monk_community = @other_monk_community
        folder.save
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @other_monk_community.name
        expect(response.results.total).to eq 1
      end
      it " removed" do
        folder.monk_community = nil
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @monk_community.name
        expect(response.results.total).to eq 1
      end
    end

    context "publication" do
      before :each do
        @publication = FactoryGirl.create :test_publication
        @other_publication = FactoryGirl.create :other_test_publication
        folder.publications << @publication
        folder.save
        Folder.__elasticsearch__.refresh_index!
      end
      it "added" do
        response = Folder.search @publication.name
        expect(response.results.total).to eq 1
      end

      it "added another" do
        folder.publications << @other_publication
        folder.save
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @other_publication.name
        expect(response.results.total).to eq 1
      end

      it "changed" do
        folder.publications.clear
        folder.publications << @other_publication
        folder.save
        Folder.__elasticsearch__.refresh_index!

        response = Folder.search @other_publication.name
        expect(response.results.total).to eq 1
      end
      it "removed" do
        folder.publications.destroy(@publication)
        folder.save

        response = Folder.search @other_publication.name
        expect(response.results.total).to eq 0
      end
    end
  end
end
