require 'rails_helper'

RSpec.describe Language, type: :model do
  before do
    @lang = FactoryGirl.build :lang
  end

  context "validations" do
    it { expect(@lang).to have_many(:language_knowledges) }
    it { expect(@lang).to have_many(:folders).through(:language_knowledges) }

    # it { should have_many :language_knowledges }
    # it { should have_many(:folders).through(:language_knowledges) }
  end

  context "callbacks" do
    before do
      @lang = FactoryGirl.create :lang
    end

    it "have to delete language knowledge after lang destroyed" do
      @language_knowledge = LanguageKnowledge.new(
        folderId: Faker::Number.number(3),
        languageId: Faker::Number.number(3),
        level: 'good'
      )
      @lang.language_knowledges << @language_knowledge

      Language.destroy(@lang.languageId)
      expect(LanguageKnowledge.all.length).to eql 0
    end

    it "should delete folder after language destroyed" do
      folder = FactoryGirl.create :folder

      @language_knowledge = LanguageKnowledge.new(
        folderId: folder.folderId,
        languageId: Faker::Number.number(3),
        level: 'nice'
      )
      @lang.language_knowledges << @language_knowledge

      expect(folder.languages.length).to eq 1

      Language.destroy(@lang.languageId)
      folder.reload
      expect(LanguageKnowledge.all.length).to eq(0)
      expect(folder.languages.length).to eq(0)
    end
  end

  context "elastic search routines" do
  end
end
