class Keyword < ActiveRecord::Base
  has_and_belongs_to_many :dissertations, :join_table => "dissertations_keywords", :foreign_key => "keywordId", association_foreign_key: 'dissertationId'
end
