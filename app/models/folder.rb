require "elasticsearch/model"
class Folder < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  index_name "folders-#{Rails.env}"

  after_touch { index_document }
  after_save  { index_document }

  has_many :dissertations, :foreign_key => "folderId", after_add: [ lambda { |f,l| f.index_document } ],
                                                       after_remove: [ lambda { |f,l| f.index_document } ]
  has_many :educations, :foreign_key => "folderId", after_add: [ lambda { |f,l| f.index_document } ],
                                                    after_remove: [ lambda { |f,l| f.index_document } ]
  has_many :experiences, :foreign_key => "folderId", after_add: [ lambda { |f,l| f.index_document } ],
                                                     after_remove: [ lambda { |f,l| f.index_document } ]
  has_many :language_knowledges, :foreign_key => "folderId", dependent: :delete_all
  has_many :languages, through: :language_knowledges, after_add: [ lambda { |f,l| f.index_document } ],
                                                      after_remove: [ lambda { |f,l| f.index_document } ]
  has_many :ordinations, :foreign_key => "folderId"
  has_many :phones, :foreign_key => "folderId"
  has_many :emails, :foreign_key => "folderId"
  has_many :publications, :foreign_key => "folderId"

  belongs_to :diocese, :foreign_key => "dioceseId"
  belongs_to :monk_community, :foreign_key => "monkCommunityId"

  def full_name
    return firstName + ' ' + lastName
  end

  def full_name=(full_name)
    prefix, first_name, last_name = full_name.matches(/^(.*\s?)([A-ZАБВГҐДЕЄЖЗИІЇЙКЛМНОПРСТУФХЦЧШЩЮЯ].+)\s(.+)$/).captures unless full_name.length == 0
    @firstName = firstName
    @lastName = last_name
  end

  def index_document
    __elasticsearch__.index_document
  end

  def as_indexed_json(options={})
    self.as_json(
      except: [:dateCreated, :dateUpdated, :dioceseId],
      include: {
        diocese: { only: :name },
        dissertations: { only:  [ :title, :educationLevel, :educationPlace, :notes ], include: { keywords: { only: :name }}},
        educations: { only: [:place, :educationLevel ]},
        experiences: { only: [:place, :position ]},
        language_knowledges: { only: :level, include: { language: { only: :name }}},
        monk_community: { only: :name },
        publications: { only: :name }
      }
    )
  end
end