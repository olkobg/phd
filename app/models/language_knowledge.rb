class LanguageKnowledge < ActiveRecord::Base
  belongs_to :folder, :foreign_key => "folderId", touch: true
  belongs_to :language, :foreign_key => "languageId"
end
