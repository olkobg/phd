class Ordination < ActiveRecord::Base
  belongs_to :folder, :foreign_key => "folderId"
  has_one :bishop, class_name: "Folder", primary_key: "bishopId", foreign_key: 'folderId'
end