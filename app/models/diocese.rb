class Diocese < ActiveRecord::Base
  has_many :folders, foreign_key: 'dioceseId', after_add: [ lambda { |d,f| f.__elasticsearch__.index_document}],
                                               after_remove: [ lambda { |d,f| f.__elasticsearch__.index_document}]
end