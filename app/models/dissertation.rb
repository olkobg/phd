class Dissertation < ActiveRecord::Base
  belongs_to :folder, :foreign_key => "folderId", touch: true
  has_one :mentor, :class_name => "Folder", :foreign_key => "folderId", :primary_key => 'mentorId'
  has_and_belongs_to_many :keywords, :join_table => "dissertations_keywords", :foreign_key => "dissertationId", :association_foreign_key => 'keywordId'
end
