class Language < ActiveRecord::Base
  has_many :language_knowledges, :foreign_key => "languageId"
  has_many :folders, :through => :language_knowledges
  after_update { self.language_knowledges.each(&:touch) }
  after_destroy { |lang| update_folder_index(lang) }
  before_destroy { |lang| save_folder_id(lang) }

  private
    def save_folder_id(lang)
      @folders = lang.folders
    end

    def update_folder_index(lang)
      LanguageKnowledge.delete_all(languageId: lang.languageId)
      @folders.each do |folder|
        folder.reload
        folder.__elasticsearch__.index_document
      end
    end
end
