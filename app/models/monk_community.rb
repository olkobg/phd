class MonkCommunity < ActiveRecord::Base
  has_many :folders, :foreign_key => 'monkCommunityId'
end
