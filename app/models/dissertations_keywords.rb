class DissertationsKeywords < ActiveRecord::Base
  belongs_to :dissertation, :foreign_key => "dissertationId"
  belongs_to :keyword, :foreign_key => "keywordId"
end