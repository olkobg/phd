module StubsHelper
  def active_class(is_active)
    if is_active
      return 'active_result'
    end
  end
end
