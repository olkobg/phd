module FolderHelper
  def phone_description(description)
    if description.nil?
      return 'folder.contact.phones.phone'
    else
      return "folder.contact.phones.#{description}"
    end
  end
end
