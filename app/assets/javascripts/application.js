// This is a manifest file thatll be compiled into application.js, which will include all the files
// listed below.
// 
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
// 
// Its not advisable to add code directly here, but if you do, itll appear at the bottom of the
// compiled file.
// 
// Read Sprockets README https://github.com/sstephenson/sprockets#sprockets-directives for details
// about supported directives.
// 

//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require_tree .