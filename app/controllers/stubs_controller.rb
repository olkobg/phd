class StubsController < ApplicationController
  def login
  end

  def search
  end

  def result
    @folder = Folder.first
  end

  def result_detail
    @email = Email.first
    @folder = Folder.find(@email.folderId) 
    render 'erorr no data' if @folder.nil?
  end

  def action
  end
end
