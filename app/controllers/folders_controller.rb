class FoldersController < ApplicationController
  before_action :authenticate_user!

  def upload_avatar
    @resource = Folder.find params[:id]
    @resource.avatar_url = params[:folder][:avatar_url]
    @resource.save
    redirect_to folder_path(params[:id])
  end

  def index
    @resource = Folder.all
    @tabs = { search: nil, result: 1 }
    render 'resource'
  end

  def show
    @resource = Folder.find(params[:id])
    @tabs = { search: nil, result_details: 1 }
    render 'resource'
  end

  def search
    @tabs = { search: 1, result: nil }
    render 'resource'
  end

  def search_index
    @tabs = { search: nil, result: 1 }
    search_response = Folder.search params[:search]
    @resource = []
    @resource = search_response.records.to_a if search_response.records.total > 0
    render 'resource'
  end
end
