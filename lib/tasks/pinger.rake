desc "This task is called by the Heroku cron add-on"
task :call_page => :environment do
  if Time.now.getlocal("+3.00").hour.between(23, 9)
    uri = URI.parse('http://phdd.herokuapp.com/')
    Net::HTTP.get(uri)
  end
end