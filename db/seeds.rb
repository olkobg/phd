# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
class Seeds

  def initialize
    @id = {}
    [
      'ADDRESS_ID',
      'CONTACT_ID',
      'DIOCESE_ID',
      'DISSERTATION_ID',
      'EDUCATION_HIGH_ID',
      'EDUCATION_LOW_ID',
      'EDUCATION_MIDDLE_ID',
      'EXPERIENCE_FIRST',
      'EXPERIENCE_SECOND',
      'FOLDER_ID',
      'FOLDER_BISHOP_ID',
      'FOLDER_MENTOR_ID',
      'KEYWORD_EAST_CHURCH_ID',
      'KEYWORD_PROTOSINKEL_ID',
      'KEYWORD_RIGHTS_ID',
      'KEYWORD_SINKEL_ID',
      'LANGUAGE_UKR',
      'LANGUAGE_ENG',
      'LANGUAGE_RUS',
      'LANGUAGE_POL',
      'MONK_COMMUNITY_ID',
      'ORDINATION_BISHOP_ID',
      'ORDINATION_DIACON_ID',
      'ORDINATION_LOW_ID',
      'ORDINATION_PRIST_ID',
      'PHONE_FAX_ID',
      'PHONE_ID',
      'PHONE_LAND_ID',
      'PHONE_MOBILE_ID',
      'PUBLICATION_FIRST',
      'PUBLICATION_SECOND',
    ].each { |value|
      @id[value.to_sym] = Faker::Number.number(3);
    }
  end

  def define_diocese
    diocese_id = @id[:DIOCESE_ID]
  end

  def define_dissertation
    @dissertations = {
      test_dissertation: {
        id: @id[:DISSERTATION_ID],
        start: Time.now - 10.years,
        finish: Time.now - 8.years,
        thesis: Time.now - 7.years,
        level: 'folder.education.levels.high.title'
      }
    }

    folder_id = @id[:FOLDER_ID]
    mentor_id = @id[:FOLDER_MENTOR_ID]

    @dissertations.each do |dissertation_name, dissertation_data|
    end
  end

  def define_dissertations_keywords
    @dissertations.each do |dissertation_name, dissertation_data|
      @keywords.each do |keyword_name, keyword_data| 
        FactoryGirl.define do
          factory (dissertation_name.to_s + '_' + keyword_name.to_s).to_sym, class: DissertationsKeywords do
            dissertationId dissertation_data[:id]
            keywordId keyword_data[:id]
          end 
        end
      end
    end
  end

  def define_experiences
    @experiences = {
      first_experience: {
        id: @id[:EXPERIENCE_FIRST],
        start: Time.now - 6.years,
        end: Time.now - 3.years
      },
      second_experience: {
        id: @id[:EXPERIENCE_SECOND],
        start: Time.now - 3.years,
        end: Time.now
      }
    }

    folder_id = @id[:FOLDER_ID]

    @experiences.each do |experience_name, experience_data|
    end
  end

  def before_keywords
    @keywords = {
      test_keyword_east_church: {
        id: @id[:KEYWORD_EAST_CHURCH_ID],
        name: 'east-church'
      },
      test_keyword_protosinkel: {
        id: @id[:KEYWORD_PROTOSINKEL_ID],
        name: 'protosinkel'
      },
      test_keyword_rights: {
        id: @id[:KEYWORD_RIGHTS_ID],
        name: 'rights'
      },
      test_keyword_sinkel: {
        id: @id[:KEYWORD_SINKEL_ID],
        name: 'sinkel'
      },
    }
  end

  def define_keywords
    @keywords.each do |keyword_name, keyword_data|
    end
  end

  def define_monk_community
    monk_community_id = @id[:MONK_COMMUNITY_ID]
    contact_id = @id[:CONTACT_ID]
  end

  def define_languages
    @languages = {
      lang_ukr: {
        id: @id[:LANGUAGE_UKR],
        name: 'Ukrainian'
      },
      lang_pol: {
        id: @id[:LANGUAGE_POL],
        name: 'polish'
      },
      lang_eng: {
        id: @id[:LANGUAGE_ENG],
        name: 'english'
      },
      lang_rus: {
        id: @id[:LANGUAGE_RUS],
        name: 'russian'
      }
    }

    @languages.each do |language_name, language_data|
    end
  end

  def define_language_knowledges
    @knowledges = {
      lang_ukr: {
        level: Faker::Number.number(1)
      },
      lang_eng: {
        level: Faker::Number.number(1)
      },
      lang_pol: {
        level: Faker::Number.number(1)
      },
      lang_rus: {
        level: Faker::Number.number(1)
      }
    }

    folder_id = @id[:FOLDER_ID]

    @languages.each do |language_name, language_data|
      current_level = @knowledges[language_name][:level]
      FactoryGirl.define do
        factory (language_name.to_s + '_' + current_level).to_sym, class: LanguageKnowledge do
          languageId language_data[:id]
          folderId folder_id
          level current_level
        end
      end
    end
  end

  def define_phones
    @phones = {
      test_phone: {
        id: @id[:PHONE_ID],
        desc: 'folder.contact.phones.phone',
      },
      test_land_phone: {
        id: @id[:PHONE_LAND_ID],
        desc: 'folder.contact.phones.land_phone' ,
      },
      test_mobile_phone: {
        id: @id[:PHONE_MOBILE_ID],
        desc: 'folder.contact.phones.mobile_phone',
      },
      test_fax: {
        id: @id[:PHONE_FAX_ID],
        desc: 'folder.contact.phones.fax',
      }
    }

    @phones.each do |phone_name, phone_data|
      folder_id = @id[:FOLDER_ID]
    end
  end

  def define_emails
    folder_id = @id[:FOLDER_ID]
  end

  def define_folders
    @folders = {
      test_user: {
        id: @id[:FOLDER_ID],
        date: Time.now - 3.month
        },
      test_bishop: {
        id: @id[:FOLDER_BISHOP_ID],
        date: Time.now - 24.month
      },
      test_mentor: {
        id: @id[:FOLDER_MENTOR_ID],
        date: Time.now - 12.month
      }
    }

    diocese_id = @id[:DIOCESE_ID]
    monk_community_id = @id[:MONK_COMMUNITY_ID]
    @folders.each do |folder_name, folder_data|
      
    end
  end

  def define_ordinations
    @ordinations = {
      test_low_ordination: {
        id: @id[:ORDINATION_LOW_ID],
        type: 'low',
        date: Time.now - 12.month
      },
      test_diacon_ordination: {
        id: @id[:ORDINATION_DIACON_ID],
        type: 'deacon',
        date: Time.now - 8.month
      },
      test_prist_ordination: {
        id: @id[:ORDINATION_PRIST_ID],
        type: 'prist',
        date: Time.now - 6.month
      },
      test_bishop_ordination: {
        id: @id[:ORDINATION_BISHOP_ID],
        type: 'bishop',
        date: Time.now - 4.month
      }
    }

    bishop_id = @id[:FOLDER_BISHOP_ID]
    folder_id = @id[:FOLDER_ID]

    @ordinations.each do |ordination_name, ordination_data|
    end
  end

  def define_education
    @educations = {
      low_education: {
        id: @id[:EDUCATION_LOW_ID],
        start: Time.now - 25.years,
        end: Time.now - 20.years,
        level: 'folder.education.levels.low'
      },
      middle_education: {
        id: @id[:EDUCATION_MIDDLE_ID],
        start: Time.now - 20.years,
        end: Time.now - 13.years,
        level: 'folder.education.levels.middle'
      },
      high_education: {
        id: @id[:EDUCATION_HIGH_ID],
        start: Time.now - 13.years,
        end: Time.now - 7.years,
        level: 'folder.education.levels.high.title'
      }
    }
    
    folder_id = @id[:FOLDER_ID]
    
    @educations.each do |education_name, education_data|
    end
  end

  def define_publications
    @publications = {
      publication_first: {
        id: @id[:PUBLICATION_FIRST],
      },
      publication_second: {
        id: @id[:PUBLICATION_SECOND],
      }
    }

    folder_id = @id[:FOLDER_ID]

    @publications.each do |publication_name, publication_data|
    end
  end

  def define_all
    before_keywords

    define_diocese
    define_dissertation
    define_dissertations_keywords
    define_education
    define_experiences
    define_emails
    define_folders
    define_keywords
    define_languages
    define_language_knowledges
    define_monk_community
    define_ordinations
    define_phones
    define_publications
  end

  def generate
    [
      @dissertations,
      @educations,
      @experiences,
      @folders,
      @keywords,
      @languages,
      @ordinations,
      @phones,
      @publications
    ].each do |field|
      field.each do |key, data|
        FactoryGirl.create key.to_sym
      end
    end

    [
      :test_email,
      :test_diocese,
      :test_monk_community
    ].each do |test_data|
      FactoryGirl.create test_data
    end

    [
      {
        first: @dissertations,
        second: @keywords
      }
    ].each do |tables|
      tables[:first].each do |first_name, first_data|
        tables[:second].each do |second_name, second_data|
          FactoryGirl.create (first_name.to_s + '_' + second_name.to_s).to_sym
        end
      end
    end

    @languages.each do |language_name, language_data|
      level =  @knowledges[language_name][:level]
      FactoryGirl.create (language_name.to_s + '_' + level.to_s).to_sym
    end

  end

end

@seeds = Seeds.new
@seeds.define_all
@seeds.generate
