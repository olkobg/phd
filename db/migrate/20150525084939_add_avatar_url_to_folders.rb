class AddAvatarUrlToFolders < ActiveRecord::Migration
  def change
    add_column :folders, :avatar_url, :string
  end
end
