class AddMonkCommunityIdAndPerpetualVowsDateToFolders < ActiveRecord::Migration
  def change
    add_column :folders, :monkCommunityId, :integer
    add_column :folders, :perpetualVowsDate, :datetime
  end
end
