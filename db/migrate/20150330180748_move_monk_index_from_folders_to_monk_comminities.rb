class MoveMonkIndexFromFoldersToMonkComminities < ActiveRecord::Migration
  def change
    remove_column :folders, :monkCommunityId, :integer
    add_column :monk_communities, :folderId, :integer
    add_index :monk_communities, :folderId
  end
end
