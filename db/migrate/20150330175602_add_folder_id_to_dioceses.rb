class AddFolderIdToDioceses < ActiveRecord::Migration
  def change
    add_column :dioceses, :folderId, :integer
    add_index :dioceses, :folderId
  end
end
