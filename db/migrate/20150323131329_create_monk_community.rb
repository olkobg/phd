class CreateMonkCommunity < ActiveRecord::Migration
  def change
    create_table :monk_communities, primary_key: :monkCommunityId, force: true do |t|
      t.string :name
      t.integer :contactId
    end
  end
end
