class CreateEmails < ActiveRecord::Migration
  def change
    
    remove_column :folders, :email
    remove_column :monk_communities, :contactId

    add_column :folders, :address, :text
    add_column :folders, :webPage, :string

    rename_column :phones, :contactId, :folderId

    drop_table :contacts
      
    create_table :emails, primary_key: :emailId, force: true do |t|
      t.string :email
      t.integer :folderId
    end

    add_index :emails, :emailId
    add_index :emails, :folderId
  end
end
