class CreateUserFolderTables < ActiveRecord::Migration
	def change

		create_table "contacts", primary_key: "contactId", force: true do |t|
			t.text   "address"
			t.string "webPageUrl"
		end

		create_table "dioceses", primary_key: "dioceseId", force: true do |t|
			t.string "name"
			t.string "description"
		end

		create_table "disseratations_keywords", id: false, force: true do |t|
			t.integer "dissertatonId"
			t.integer "keywordId"
		end

		add_index "disseratations_keywords", ["dissertatonId"], name: "fk_disseratations_keywords_dissertatons1_idx", using: :btree
		add_index "disseratations_keywords", ["keywordId"], name: "fk_disseratations_keywords_keywords1_idx", using: :btree

		create_table "dissertations", primary_key: "dissertationId", force: true do |t|
			t.integer  "folderId"
			t.datetime "startDate"
			t.datetime "finishDate"
			t.datetime "thesisDate"
			t.string   "title"
			t.string   "educationLevel"
			t.integer  "mentorId"
			t.string   "educationPlace"
		end

		add_index "dissertations", ["folderId"], name: "fk_dissertatons_folders1_idx", using: :btree
		add_index "dissertations", ["mentorId"], name: "fk_dissertatons_folders2_idx", using: :btree

		create_table "educations", primary_key: "educationId", force: true do |t|
			t.integer  "folderId"
			t.datetime "dateStart"
			t.datetime "dateEnd"
			t.string   "place"
			t.string   "educationLevel"
		end

		add_index "educations", ["folderId"], name: "fk_educations_folders1_idx", using: :btree

		create_table "experiences", primary_key: "experienceId", force: true do |t|
			t.integer  "folderId"
			t.datetime "startDate"
			t.datetime "endDate"
			t.text     "place"
			t.text     "position"
		end

		add_index "experiences", ["folderId"], name: "fk_experience_folders1_idx", using: :btree

		create_table "folders", primary_key: "folderId", force: true do |t|
			t.string   "firstName"
			t.string   "lastName"
			t.string   "email" 
			t.string  "sex"
			t.integer  "dioceseId", null: false
			t.integer  "contactId"
			t.text     "note"
			t.datetime "dateCreated"
			t.datetime "dateUpdated"
		end

		add_index "folders", ["contactId"], name: "fk_folders_contacts1_idx", using: :btree
		add_index "folders", ["dioceseId"], name: "fk_folders_dioceses_idx", using: :btree

		create_table "folders_languages", id: false, force: true do |t|
			t.integer "folderId"
			t.integer "languageId"
			t.string  "level"
		end

		add_index "folders_languages", ["folderId"], name: "fk_folders_languages_folders1_idx", using: :btree
		add_index "folders_languages", ["languageId"], name: "fk_folders_languages_languages1_idx", using: :btree

		create_table "keywords", primary_key: "keywordId", force: true do |t|
			t.string "name"
		end

		create_table "languages", primary_key: "languageId", force: true do |t|
			t.string "name"
		end

		create_table "ordinations", primary_key: "ordinationId", force: true do |t|
			t.string   "ordinatonsType"
			t.integer  "bishopId"
			t.datetime "date"
			t.integer  "folderId"
		end

		add_index "ordinations", ["bishopId"], name: "fk_ordinations_folders1_idx", using: :btree
		add_index "ordinations", ["folderId"], name: "fk_ordinations_folders2_idx", using: :btree

		create_table "phones", primary_key: "phoneId", force: true do |t|
			t.integer "contactId"
			t.string  "phoneNumber"
			t.string  "description"
		end

		add_index "phones", ["contactId"], name: "fk_phones_contacts1_idx", using: :btree

		create_table "publications", primary_key: "publicationId", force: true do |t|
			t.integer  "folderId"
			t.string   "name"
			t.datetime "date"
		end

		add_index "publications", ["folderId"], name: "fk_publications_folders1_idx", using: :btree

	end
end
