class RenameFieldsForEducation < ActiveRecord::Migration
  def change
    change_table :educations do |t|
      t.rename :dateStart, :startDate
      t.rename :dateEnd,   :endDate
    end
  end
end
