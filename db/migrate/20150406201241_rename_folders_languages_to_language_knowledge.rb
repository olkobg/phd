class RenameFoldersLanguagesToLanguageKnowledge < ActiveRecord::Migration
  def change
    rename_table :folders_languages, :language_knowledges
  end
end
