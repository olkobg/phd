class AddDioceseAndMonkCommunityToFolder < ActiveRecord::Migration
  def change
    add_column :folders, :monkCommunityId, :integer
    add_index :folders, :monkCommunityId
    add_column :folders, :dioceseId, :integer
    add_index :folders, :dioceseId
    remove_column :dioceses, :folderId
    remove_column :monk_communities, :folderId
  end
end
