class UpdateFolderToContactsConnection < ActiveRecord::Migration
  def change
    remove_column :folders, :contactId, :integer
    add_column :contacts, :folderId, :integer
    add_index :contacts, :folderId
  end
end