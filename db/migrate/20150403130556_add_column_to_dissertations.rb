class AddColumnToDissertations < ActiveRecord::Migration
  def change
    add_column :dissertations, :notes, :text
  end
end
