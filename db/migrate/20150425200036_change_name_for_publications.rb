class ChangeNameForPublications < ActiveRecord::Migration
  def change
    change_column :publications, :name, :text
  end
end
