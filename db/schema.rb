# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150525084939) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dioceses", primary_key: "dioceseId", force: true do |t|
    t.string "name"
    t.string "description"
  end

  create_table "dissertations", primary_key: "dissertationId", force: true do |t|
    t.integer  "folderId"
    t.datetime "startDate"
    t.datetime "finishDate"
    t.datetime "thesisDate"
    t.string   "title"
    t.string   "educationLevel"
    t.integer  "mentorId"
    t.string   "educationPlace"
    t.text     "notes"
  end

  add_index "dissertations", ["folderId"], name: "fk_dissertatons_folders1_idx", using: :btree
  add_index "dissertations", ["mentorId"], name: "fk_dissertatons_folders2_idx", using: :btree

  create_table "dissertations_keywords", id: false, force: true do |t|
    t.integer "dissertationId"
    t.integer "keywordId"
  end

  add_index "dissertations_keywords", ["dissertationId"], name: "fk_disseratations_keywords_dissertatons1_idx", using: :btree
  add_index "dissertations_keywords", ["keywordId"], name: "fk_disseratations_keywords_keywords1_idx", using: :btree

  create_table "educations", primary_key: "educationId", force: true do |t|
    t.integer  "folderId"
    t.datetime "startDate"
    t.datetime "endDate"
    t.string   "place"
    t.string   "educationLevel"
  end

  add_index "educations", ["folderId"], name: "fk_educations_folders1_idx", using: :btree

  create_table "emails", primary_key: "emailId", force: true do |t|
    t.string  "email"
    t.integer "folderId"
  end

  add_index "emails", ["emailId"], name: "index_emails_on_emailId", using: :btree
  add_index "emails", ["folderId"], name: "index_emails_on_folderId", using: :btree

  create_table "experiences", primary_key: "experienceId", force: true do |t|
    t.integer  "folderId"
    t.datetime "startDate"
    t.datetime "endDate"
    t.text     "place"
    t.text     "position"
  end

  add_index "experiences", ["folderId"], name: "fk_experience_folders1_idx", using: :btree

  create_table "folders", primary_key: "folderId", force: true do |t|
    t.string   "firstName"
    t.string   "lastName"
    t.string   "sex"
    t.text     "note"
    t.datetime "dateCreated"
    t.datetime "dateUpdated"
    t.datetime "perpetualVowsDate"
    t.integer  "monkCommunityId"
    t.integer  "dioceseId"
    t.text     "address"
    t.string   "webPage"
    t.string   "avatar_url"
  end

  add_index "folders", ["dioceseId"], name: "index_folders_on_dioceseId", using: :btree
  add_index "folders", ["monkCommunityId"], name: "index_folders_on_monkCommunityId", using: :btree

  create_table "keywords", primary_key: "keywordId", force: true do |t|
    t.string "name"
  end

  create_table "language_knowledges", id: false, force: true do |t|
    t.integer "folderId"
    t.integer "languageId"
    t.string  "level"
  end

  add_index "language_knowledges", ["folderId"], name: "fk_folders_languages_folders1_idx", using: :btree
  add_index "language_knowledges", ["languageId"], name: "fk_folders_languages_languages1_idx", using: :btree

  create_table "languages", primary_key: "languageId", force: true do |t|
    t.string "name"
  end

  create_table "monk_communities", primary_key: "monkCommunityId", force: true do |t|
    t.string "name"
  end

  create_table "ordinations", primary_key: "ordinationId", force: true do |t|
    t.string   "ordinationType"
    t.integer  "bishopId"
    t.datetime "date"
    t.integer  "folderId"
  end

  add_index "ordinations", ["bishopId"], name: "fk_ordinations_folders1_idx", using: :btree
  add_index "ordinations", ["folderId"], name: "fk_ordinations_folders2_idx", using: :btree

  create_table "phones", primary_key: "phoneId", force: true do |t|
    t.integer "folderId"
    t.string  "phoneNumber"
    t.string  "description"
  end

  add_index "phones", ["folderId"], name: "fk_phones_contacts1_idx", using: :btree

  create_table "publications", primary_key: "publicationId", force: true do |t|
    t.integer  "folderId"
    t.text     "name"
    t.datetime "date"
  end

  add_index "publications", ["folderId"], name: "fk_publications_folders1_idx", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
